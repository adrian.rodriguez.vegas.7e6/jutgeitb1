fun main() {
    println("Benvingut a jutge!")
    val llistaProblema: MutableList<Problema> = mutableListOf<Problema>()
    val problemes: MutableList<Problema> = mutableListOf(
        Problema("Suma de dos números",
            Pair("Entrada: 3 4", "Sortida: 7"),
            "3 4"),

        Problema("Multiplicació de tres números",
            Pair("Entrada: 5 9 3", "Sortida: 135"),
            "5 9 3"),

        Problema("Resta de dos números",
            Pair("Entrada: 124 95", "Sortida: 29"),
            "124 95"),

        Problema("Divisió de dos números",
            Pair("Entrada: 3450 4", "Sortida: 862.5"),
            "3450 4"),

        Problema("Potència d'un número",
            Pair("Entrada: 2^4","Sortida: 16"),
            "2^4")
    )

    for (problema in problemes){
        problema.mostrarEnunciat()
        println("Vols resoldre'l? [S/n]")
        var resposta = readLine()!!.toString()

        if (resposta == "S"){
            problema.mostrarEnunciatPerResoldre()
            println("Introdueix el resultat: ")
            var solucio = readLine()!!.toDouble()
        }

    }
    println("Fins un altre!")
}